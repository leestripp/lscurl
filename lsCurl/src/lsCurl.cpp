#include "lsCurl.h"

lsCurl::lsCurl() : lsThread()
{
	m_mode = LSM_NONE;
	m_curl = NULL;
	reset();
}

void lsCurl::reset()
{
	m_stop = false;
	// progress
	m_upload_now = 0;
	m_upload_total = 0;
	m_download_now = 0;
	m_download_total = 0;
}

//********************
// Work Thread.

void lsCurl::work()
{
	// debug
	cout << "Thread: lsCurl::work() start..." << endl;
	
	// reset values
	reset();
	
	// Check mode and start work.
	switch( m_mode )
	{
		case LSM_NONE:
		{
			cerr << "ERROR: No mode set..." << endl;
			break;
		}
		
		case LSM_GET:
		{
			cout << "Mode: LSM_GET" << endl;
			get();
			break;
		}
		
		case LSM_DOWNLOAD:
		{
			cout << "Mode: LSM_DOWNLOAD" << endl;
			download();
			break;
		}
		
		default:
		{
			cerr << "ERROR: Unknown mode set..." << endl;
			break;
		}
	}
	
	// signal we are done.
	set_done(true);
	notify_done.emit( this );
}

void lsCurl::get()
{
    CURLcode res;
	m_dataBuffer.clear();
	m_pageBuffer = string();
	
    m_curl = curl_easy_init();
    if( m_curl )
	{
		// options
        curl_easy_setopt( m_curl, CURLOPT_URL, m_url.c_str() );
        curl_easy_setopt( m_curl, CURLOPT_WRITEFUNCTION, WriteCallback );
        curl_easy_setopt( m_curl, CURLOPT_WRITEDATA, this );
		curl_easy_setopt( m_curl, CURLOPT_FOLLOWLOCATION, 1L );
		// progress
		curl_easy_setopt( m_curl, CURLOPT_XFERINFOFUNCTION, xferinfo );
		curl_easy_setopt( m_curl, CURLOPT_XFERINFODATA, this );
		curl_easy_setopt( m_curl, CURLOPT_NOPROGRESS, 0L);
		
		res = curl_easy_perform( m_curl );
		if( res == CURLE_OK )
		{
			m_pageBuffer = string( m_dataBuffer.begin(), m_dataBuffer.end() );
		}
		
		// cleanup
		curl_easy_cleanup( m_curl );
		m_curl = NULL;
	}
}

void lsCurl::download()
{
    CURLcode res;
	m_dataBuffer.clear();
	
    m_curl = curl_easy_init();
    if( m_curl )
	{
		// options
        curl_easy_setopt( m_curl, CURLOPT_URL, m_url.c_str() );
        curl_easy_setopt( m_curl, CURLOPT_WRITEFUNCTION, WriteCallback );
        curl_easy_setopt( m_curl, CURLOPT_WRITEDATA, this );
		curl_easy_setopt( m_curl, CURLOPT_FOLLOWLOCATION, 1L );
		// progress
		curl_easy_setopt( m_curl, CURLOPT_XFERINFOFUNCTION, xferinfo );
		curl_easy_setopt( m_curl, CURLOPT_XFERINFODATA, this );
		curl_easy_setopt( m_curl, CURLOPT_NOPROGRESS, 0L);
		
		res = curl_easy_perform( m_curl );
		if( res == CURLE_OK )
		{
			// Save to file.
			ofstream out( m_filename, ios::binary );
			if( out.is_open() )
			{
				for( const char& ch : m_dataBuffer)
				{
					out << ch;
				}
				out.close();
			}
		}
		
		// cleanup
		curl_easy_cleanup( m_curl );
		m_curl = NULL;
	}
}

void lsCurl::writeData( char ch )
{
	m_dataBuffer.push_back( ch );
}


// *********************
// Callbacks

size_t lsCurl::WriteCallback( void* contents, size_t size, size_t nmemb, lsCurl* lscurl )
{
	char *buffer = (char *)contents;
	for( size_t i=0; i< size * nmemb; i++ )
	{
		lscurl->writeData( buffer[i] );
	}
	// emit update signal
	lscurl->notify_update.emit( lscurl );
	
    return size * nmemb;
}

// CURLOPT_XFERINFOFUNCTION progress callback

int lsCurl::xferinfo( void *p, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow )
{
	lsCurl *lscurl = (lsCurl *)p;
	
	lscurl->m_upload_now = (ulong)ulnow;
	lscurl->m_upload_total = (ulong)ultotal;
	lscurl->m_download_now = (ulong)dlnow;
	lscurl->m_download_total = (ulong)dltotal;
	
	return 0;
}

