#ifndef LSTHREAD_H
#define LSTHREAD_H

#include <iostream>
#include <thread>
#include <mutex>
// sigc++
#include <sigc++/sigc++.h>

using namespace std;

class lsThread : public sigc::trackable
{
public:
	lsThread();
	virtual ~lsThread();
	
	// Starts the work thread.
	void start();
	void join();
	
	// what your thread will do.
	virtual void work() = 0;
	
	// signals.
	sigc::signal<void(lsThread *)> notify_update;
	sigc::signal<void(lsThread *)> notify_done;
	
	// get set
	ulong get_type()
	{
		return m_type;
	}
	void set_type( ulong val )
	{
		m_type = val;
	}
	
	bool get_done()
	{
		return m_done;
	}
	void set_done( bool val )
	{
		m_done = val;
	}
	
private:
	thread *m_worker;
	
	ulong m_type;
	bool m_done;
};

#endif // LSTHREAD_H