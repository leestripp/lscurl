#include "lsThread.h"

lsThread::lsThread()
{
	m_worker = nullptr;
	m_type = 0;
	m_done = false;
}

lsThread::~lsThread()
{
	// cleanup
	join();
}


void lsThread::start()
{
	// check running
	if( m_worker ) return;
	
	// Start the work thread.
	m_worker = new thread( &lsThread::work, this );
}

void lsThread::join()
{
	if( m_worker )
	{
		if( m_worker->joinable() ) m_worker->join();
		delete m_worker;
		m_worker = nullptr;
	}
}
