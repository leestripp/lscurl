#ifndef LSCURL_H
#define LSCURL_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
// curl
#include <curl/curl.h>

// thread
#include "lsThread.h"

// Modes
const int LSM_NONE		= 0;
const int LSM_GET		= 1;
const int LSM_DOWNLOAD	= 2;

using namespace std;

class lsCurl : public lsThread
{
public:
	lsCurl();
	
	void reset();
	void work();
	void get();
	void download();
	
	void writeData( char ch );
	
	// get set
	void set_stop( bool val )
	{
		m_stop = val;
	}
	
	string get_pageBuffer()
	{
		return m_pageBuffer;
	}
	
	// settings.
	int m_mode;
	string m_url;
	string m_filename;
	
	// progress
	ulong m_upload_now;
	ulong m_upload_total;
	ulong m_download_now;
	ulong m_download_total;
	
private:
	// Callbacks
	static size_t WriteCallback( void* contents, size_t size, size_t nmemb, lsCurl* lscurl );
	static int xferinfo( void *p, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow );
	
	// data
	vector<char> m_dataBuffer;
	
	bool m_stop;
	CURL* m_curl;
	string m_pageBuffer;
};

#endif // LSCURL_H

