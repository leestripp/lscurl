#ifndef TEST2_H
#define TEST2_H

#include <iostream>
#include <string>

#include "lsCurl.h"

const ulong LSTT_NONE	= 0;
const ulong LSTT_CURL	= 1;

using namespace std;

class test2
{
public:
	test2();
	
	void run();
	
	// slots
	void thread_update( lsThread *th );
	void thread_done( lsThread *th );
	
private:
	lsCurl m_lscurl;
};

#endif // TEST2_H
