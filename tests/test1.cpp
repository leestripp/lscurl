#include <iostream>
#include <string>

#include "lsCurl.h"

const ulong LSTT_NONE	= 0;
const ulong LSTT_CURL	= 1;

using namespace std;

void thread_done( lsThread *th )
{
	lsCurl *lscurl = (lsCurl *)th;
	
	cout << "[DONE]" << endl;
	switch( lscurl->m_mode )
	{
		case LSM_GET:
		{
			cout << lscurl->get_pageBuffer() << endl;
			cout << "^^^ Test GET ^^^" << endl;
			cout << "URL : " << lscurl->m_url << endl;
			break;
		}
		
		case LSM_DOWNLOAD:
		{
			cout << "Test DOWNLOAD" << endl;
			cout << "URL : " << lscurl->m_url << endl;
			break;
		}
	}
}

void thread_update( lsThread *th )
{
	// lsCurl *lscurl = (lsCurl *)th;
	
	cout << ".";
}

int main()
{
	lsCurl lscurl;
	lscurl.set_type( LSTT_CURL );
	// connect
	lscurl.notify_done.connect( sigc::ptr_fun(thread_done) );
	lscurl.notify_update.connect( sigc::ptr_fun(thread_update) );
	
	// Test get
	
	lscurl.m_mode = LSM_GET;
	lscurl.m_url = "https://en.wikipedia.org/wiki/Linux";
	lscurl.start();
	// wait.
	lscurl.join();
	
	// test download
	
	lscurl.m_mode = LSM_DOWNLOAD;
	lscurl.m_url = "https://gitlab.com/leestripp/lschatai/-/raw/main/screenshots/lsChatAI.png";
	lscurl.m_filename = "lsChatAI.png";
	lscurl.start();
	// wait.
	lscurl.join();
	
	return 0;
}
