#include "test2.h"

test2::test2()
{
	// only for user reference.
	m_lscurl.set_type( LSTT_CURL );
	// connect
	m_lscurl.notify_done.connect( sigc::mem_fun(*this, &test2::thread_done) );
	m_lscurl.notify_update.connect( sigc::mem_fun(*this, &test2::thread_update) );
}


void test2::run()
{
	// Test get
	
	m_lscurl.m_mode = LSM_GET;
	m_lscurl.m_url = "https://en.wikipedia.org/wiki/Linux";
	m_lscurl.start();
	// wait.
	m_lscurl.join();
	
	// test download
	
	m_lscurl.m_mode = LSM_DOWNLOAD;
	m_lscurl.m_url = "https://gitlab.com/leestripp/lschatai/-/raw/main/screenshots/lsChatAI.png";
	m_lscurl.m_filename = "lsChatAI.png";
	m_lscurl.start();
	// wait.
	m_lscurl.join();
}

void test2::thread_done( lsThread *th )
{
	lsCurl *lscurl = (lsCurl *)th;
	
	cout << "[DONE]" << endl;
	switch( lscurl->m_mode )
	{
		case LSM_GET:
		{
			cout << lscurl->get_pageBuffer() << endl;
			cout << "^^^ Test GET ^^^" << endl;
			cout << "URL : " << lscurl->m_url << endl;
			break;
		}
		
		case LSM_DOWNLOAD:
		{
			cout << "Test DOWNLOAD" << endl;
			cout << "URL : " << lscurl->m_url << endl;
			break;
		}
	}
}

void test2::thread_update( lsThread *th )
{
	lsCurl *lscurl = (lsCurl *)th;
	
	cout << "Down: " << lscurl->m_download_now;
	cout << " of " << lscurl->m_download_total << endl;
}

// ****************
// Main

int main()
{
	test2 app;
	
	// example class implementation.
	app.run();
	
	return 0;
}
